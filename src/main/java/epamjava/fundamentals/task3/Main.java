package epamjava.fundamentals.task3;

import java.util.Calendar;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.print("Year: ");
        int year = Integer.parseInt(scan.nextLine());

        System.out.print("Month: ");
        int month = Integer.parseInt(scan.nextLine());

        try {
            System.out.println(String.format(
                    "%d %s - %s year",
                    year,
                    getMonthAsString(month),
                    isLeapYear(year) ? "leap" : "non-leap"
            ));
        } catch (IllegalArgumentException ex) {
            System.err.println(ex.getMessage());
        }

        scan.close();
    }

    private static boolean isPositiveNumber(int number) {
        return number >= 0;
    }

    private static boolean isLeapYear(int year) throws IllegalArgumentException {
        if (!isPositiveNumber(year)) {
            throw new IllegalArgumentException("Year must be a positive number");
        }

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        return calendar.getActualMaximum(Calendar.DAY_OF_YEAR) > 365;
    }

    private static String getMonthAsString(int month) throws IllegalArgumentException {
        if (!isPositiveNumber(month)) {
            throw new IllegalArgumentException("Month must be a positive number");
        }

        switch (month) {
            case 1: return "January";
            case 2: return "February";
            case 3: return "March";
            case 4: return "April";
            case 5: return "May";
            case 6: return "June";
            case 7: return "Jule";
            case 8: return "August";
            case 9: return "September";
            case 10: return "October";
            case 11: return "November";
            case 12: return "December";
            default: throw new IllegalArgumentException("Month must be in the range of 1 to 12");
        }
    }
}
